import requests
import json
from .keys import PEXELS_API_KEY


def location_picture(city, state):
    try:
        url = "https://api.pexels.com/v1/search"
        params = {"query": city + " " + state, "per_page": 1}
        headers = {"Authorization": PEXELS_API_KEY}

        res = requests.get(url, params=params, headers=headers)
        unencoded = json.loads(res.content)

        url = unencoded["photos"][0]["url"]
        return {"picture_url": url}
    except (KeyError, IndexError):
        return {"picture_url": None}

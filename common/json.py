from json import JSONEncoder
from django.db.models import QuerySet
from datetime import datetime


class DateEncoder(JSONEncoder):
    def default(self, o):
        if isinstance(o, datetime):
            return o.isoformat()
        else:
            return super().default(o)


class QuerySetEncoder(JSONEncoder):
    def default(self, o):
        if isinstance(o, QuerySet):
            return list(o)
        else:
            return super().default(o)


class ModelEncoder(DateEncoder, QuerySetEncoder, JSONEncoder):
    encoders = {}

    def default(self, o):
        if isinstance(o, self.model):
            d = {}
            if hasattr(o, "get_api_url"):
                d["href"] = o.get_api_url()
            for property in self.properties:
                value = getattr(o, property)
                if property in self.encoders:
                    encoder = self.encoders[property]
                    value = encoder.default(value)
                d[property] = value
            d.update(self.get_extra_data(o))
            return d
        else:
            return super().default(o)

    def get_extra_data(self, o):
        return {}

    # These are my helper classes (different from model classes in my models.py files) for my dream encoders,
    # which I have created in the api_views.py for my api functions.
    # When I create my encoders, they will inherit the attributes which I have outlined in my helper class,
    # take that data, encode it for my api function, to allow JSON to have the data in needs in the format it requires
    # to execute a request (derived from my functions, which inherit their information from their class models)

    # My helper class really is just the ModelEncoder - the other two specifically deal with attributes that have dates
    # (and need things formatted a certain way), and queryset (which has been created to pull a list of something)
    # My main helper class takes the other mini helper classes we created as parameters and inherits from them so it will encompass
    # everything necessary when going into the encoder functions

    # Since we are not usinh the default JSON encoders but creating our own, this is why we must go through this process.

    # The isinstance() function returns True if the specified object is of the specified type, otherwise False

    # Using the getattr function is the way we can get the value of the property of the object by its name.
    # The name is stored in the variable defined by the for loop

    # The super() function in Python makes class inheritance more manageable and extensible.
    # The function returns a temporary object that allows reference to a parent class by the keyword super.
